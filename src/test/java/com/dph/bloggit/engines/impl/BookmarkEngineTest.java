package com.dph.bloggit.engines.impl;

import com.dph.bloggit.engines.IBookmarkEngine;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class BookmarkEngineTest {

    private IBookmarkEngine bookmarkEngine = new BookmarkEngine();

    private String urlWithNoPrefix = "www.testing.com";
    private String urlWithPrefix = "http://www.testing.org";
    private String uppercaseUrlWithNoPrefix = "WWW.TESTING-TEST.COM";
    private String uppercaseUrlWithPrefix = "HTTP://WWW.TESTING.IO";

    @Test
    public void getFullUrl() {
        String result = bookmarkEngine.getFullUrl(urlWithNoPrefix);
        assertTrue(result.equals("http://www.testing.com"));
    }

    @Test
    public void getFullUrl_givenAlreadyFullUrl() {
        String result = bookmarkEngine.getFullUrl(urlWithPrefix);
        assertTrue(result.equals("http://www.testing.org"));
    }

    @Test
    public void getFullUrl_givenUppercaseUrl() {
        String result = bookmarkEngine.getFullUrl(uppercaseUrlWithNoPrefix);
        assertTrue(result.equals("http://www.testing-test.com"));
    }

    @Test
    public void getFullUrl_givenUppercaseAlreadyFullUrl() {
        String result = bookmarkEngine.getFullUrl(uppercaseUrlWithPrefix);
        assertTrue(result.equals("http://www.testing.io"));
    }
}