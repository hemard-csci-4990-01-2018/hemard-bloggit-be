package com.dph.bloggit.managers.impl;

import com.dph.bloggit.accessors.IBookmarkAccessor;
import com.dph.bloggit.converters.IBookmarkConverter;
import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.views.ViewBookmark;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import javax.swing.text.View;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookmarkManagerTest {

    @Mock
    private IBookmarkAccessor bookmarkAccessor;

    @Mock
    private IBookmarkConverter bookmarkConverter;

    @InjectMocks
    private BookmarkManager bookmarkManager;

    private DomainBookmark domainBookmark;
    private ViewBookmark viewBookmark;

    @Before
    public void setUp() throws Exception {
        domainBookmark = new DomainBookmark();
        viewBookmark = new ViewBookmark();
    }

    @Test
    public void getAllBookmarks() {
        when(bookmarkAccessor.findAll())
                .thenReturn(Arrays.asList(domainBookmark));
        when(bookmarkConverter.domainToView(any(DomainBookmark.class)))
                .thenReturn(viewBookmark);
        List<ViewBookmark> results = bookmarkManager.getAllBookmarks();
        assertTrue(results.size() == 1);
        assertEquals(viewBookmark, results.get(0));
    }

    @Test
    public void getBookmarkById() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(domainBookmark);
        when(bookmarkConverter.domainToView(any(DomainBookmark.class)))
                .thenReturn(viewBookmark);
        ViewBookmark result = bookmarkManager.getBookmarkById(1L);
        assertEquals(viewBookmark, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getBookmarkById_EntityNotFound() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(null);
        ViewBookmark result = bookmarkManager.getBookmarkById(1L);
    }

    @Test
    public void getVisibleBookmarks() {
        when(bookmarkAccessor.findAllByIsVisible(anyBoolean()))
                .thenReturn(Arrays.asList(domainBookmark));
        when(bookmarkConverter.domainToView(any(DomainBookmark.class)))
                .thenReturn(viewBookmark);
        List<ViewBookmark> results = bookmarkManager
                .getVisibleBookmarks(true);
        assertTrue(results.size() == 1);
        assertEquals(viewBookmark, results.get(0));
    }

    @Test
    public void createBookmark() {
        when(bookmarkConverter.viewToDomain(any(ViewBookmark.class)))
                .thenReturn(domainBookmark);
        when(bookmarkAccessor.save(any(DomainBookmark.class)))
                .thenReturn(domainBookmark);
        when(bookmarkConverter.domainToView(any(DomainBookmark.class)))
                .thenReturn(viewBookmark);
        ViewBookmark result = bookmarkManager.createBookmark(viewBookmark);
        assertEquals(viewBookmark, result);
    }

    @Test
    public void updateBookmark() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(domainBookmark);
        when(bookmarkConverter.viewToDomain(any(ViewBookmark.class)))
                .thenReturn(domainBookmark);
        when(bookmarkAccessor.save(any(DomainBookmark.class)))
                .thenReturn(domainBookmark);
        when(bookmarkConverter.domainToView(any(DomainBookmark.class)))
                .thenReturn(viewBookmark);
        viewBookmark.setBookmarkID(1L);
        ViewBookmark result = bookmarkManager
                .updateBookmark(1L, viewBookmark);
        assertEquals(viewBookmark, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateBookmark_EntityNotFound() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(null);
        bookmarkManager.updateBookmark(1L, viewBookmark);
    }

    @Test(expected = InvalidParameterException.class)
    public void updateBookmark_InvalidParameter() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(domainBookmark);
        viewBookmark.setBookmarkID(2L);
        ViewBookmark result = bookmarkManager
                .updateBookmark(1L, viewBookmark);
    }

    @Test
    public void deleteBookmark() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(domainBookmark);
        doNothing().when(bookmarkAccessor).delete(anyLong());
        when(bookmarkConverter.domainToView(any(DomainBookmark.class)))
                .thenReturn(viewBookmark);
        ViewBookmark result = bookmarkManager.deleteBookmark(1L);
        assertEquals(viewBookmark, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteBookmark_EntityNotFound() {
        when(bookmarkAccessor.findOne(anyLong()))
                .thenReturn(null);
        ViewBookmark result = bookmarkManager.deleteBookmark(1L);
    }
}