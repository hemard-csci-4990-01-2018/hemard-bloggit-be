package com.dph.bloggit.converters.impl;

import com.dph.bloggit.accessors.IBookmarkAccessor;
import com.dph.bloggit.converters.ILocalDateTimeConverter;
import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.engines.IBookmarkEngine;
import com.dph.bloggit.views.ViewBookmark;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookmarkConverterTest {

    @Mock
    private ILocalDateTimeConverter localDateTimeConverter;

    @Mock
    private IBookmarkAccessor bookmarkAccessor;

    @Mock
    private IBookmarkEngine bookmarkEngine;

    @InjectMocks
    private BookmarkConverter bookmarkConverter;

    private DomainBookmark domainBookmark;
    private ViewBookmark viewBookmark;
    private LocalDateTime ldtFebruaryTwentyThird2018;
    private Long februaryTwentyThird2018;

    @Before
    public void setUp() throws Exception {
        domainBookmark = new DomainBookmark();
        viewBookmark = new ViewBookmark();

        februaryTwentyThird2018 = 1519365600000L; // 2018-02-23 in milliseconds since epoch
        ldtFebruaryTwentyThird2018 = LocalDateTime.of(2018, 2, 23, 0, 0);;
    }

    @Test
    public void viewToDomain_viewHasNoBookmarkId() {
        setUpDummyViewBookmark();
        assertNull(viewBookmark.getBookmarkID());

        DomainBookmark result = bookmarkConverter.viewToDomain(viewBookmark);
        assertNotNull(result.getLastUpdated());
        assertNotNull(result.getCreatedOn());
        assertTrue(result.getCreatedOn().equals(result.getLastUpdated()));
    }

    @Test
    public void viewToDomain_viewDoesHaveBookmarkId() {
        viewBookmark.setBookmarkID(1000L);
        setUpDummyDomainBookmark();
        when(bookmarkAccessor.findOne(anyLong())).thenReturn(domainBookmark);

        DomainBookmark result = bookmarkConverter.viewToDomain(viewBookmark);
        assertEquals(domainBookmark.getCreatedOn(), result.getCreatedOn());

        // Ensure the lastUpdated attribute has been changed
        assertNotEquals(domainBookmark.getLastUpdated(), result.getLastUpdated());
    }

    @Test
    public void domainToView() {
        setUpDummyDomainBookmark();
        when(localDateTimeConverter.convertLocalDateTimeToLong(
                any(LocalDateTime.class)))
                .thenReturn(februaryTwentyThird2018);
        when(bookmarkEngine.getFullUrl(anyString()))
                .thenReturn("http://www.fullurl.com");

        ViewBookmark result = bookmarkConverter.domainToView(domainBookmark);
        assertEquals(februaryTwentyThird2018, result.getLastUpdated());
        assertEquals("http://www.fullurl.com", result.getFullUrl());
    }

    private void setUpDummyDomainBookmark() {
        domainBookmark.setBookmarkID(1000L);
        domainBookmark.setTitle("Title");
        domainBookmark.setUrl("www.testurl.com");
        domainBookmark.setVisible(true);
        domainBookmark.setPriority(1);
        domainBookmark.setCreatedBy("Test User");
        domainBookmark.setCreatedOn(ldtFebruaryTwentyThird2018);
        domainBookmark.setLastUpdated(ldtFebruaryTwentyThird2018);
    }

    private void setUpDummyViewBookmark() {
        viewBookmark.setTitle("Some Title");
        viewBookmark.setUrl("www.someurl.com");
        viewBookmark.setVisible(true);
        viewBookmark.setPriority(10);
        viewBookmark.setCreatedBy("Another User");
    }
}