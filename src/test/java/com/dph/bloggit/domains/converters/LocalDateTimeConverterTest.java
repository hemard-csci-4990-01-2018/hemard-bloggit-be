package com.dph.bloggit.domains.converters;

import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

public class LocalDateTimeConverterTest {

    private LocalDateTimeConverter localDateTimeConverter = new LocalDateTimeConverter();

    private LocalDateTime localDateTime;
    private Date date;

    @Before
    public void setUp() throws Exception {
        Instant instant = Instant.ofEpochMilli(1539234000000L);
        date = Date.from(instant);
        localDateTime = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    @Test
    public void convertToDatabaseColumn() {
        Date result = localDateTimeConverter.convertToDatabaseColumn(localDateTime);
        assertThat(date.compareTo(result), is(0));
    }

    @Test
    public void convertToDatabaseColumn_NullValue() {
        Date result = localDateTimeConverter.convertToDatabaseColumn(null);
        assertThat(result, is(nullValue()));
    }

    @Test
    public void convertToEntityAttribute() {
        LocalDateTime result = localDateTimeConverter.convertToEntityAttribute(date);
        assertThat(localDateTime.compareTo(result), is(0));
    }

    @Test
    public void convertToEntityAttribute_NullValue() {
        Date result = localDateTimeConverter.convertToDatabaseColumn(null);
        assertThat(result, is(nullValue()));
    }

}