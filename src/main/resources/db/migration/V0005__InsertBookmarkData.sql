INSERT INTO bookmarks (title, url, is_visible, priority) VALUES
  ('UNO Website', 'www.uno.edu', TRUE, 100),
  ('Duck Duck Go', 'www.duckduckgo.com', FALSE, 40),
  ('UNO Email', 'owa.uno.edu', TRUE, 90),
  ('Spring Documentation', 'spring.io/docs', FALSE, 70),
  ('Maven Documentation', 'maven.apache.org/guides/index.html', FALSE, 80),
  ('Angular Documentation', 'angular.io/docs', FALSE, 60),
  ('UNO Moodle', 'uno.mrooms3.net', TRUE, 50),
  ('UNO Gitlab', 'gitlab.cs.uno.edu', TRUE, 30),
  ('Gitlab', 'gitlab.com', FALSE, 20),
  ('Stack Overflow', 'stackoverflow.com', TRUE, 10);