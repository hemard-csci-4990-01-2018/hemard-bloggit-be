CREATE TABLE bookmarks (
  bookmark_id   BIGSERIAL PRIMARY KEY,
  title         TEXT NOT NULL,
  url           TEXT,
  is_visible    BOOLEAN NOT NULL DEFAULT FALSE,
  priority      INT NOT NULL DEFAULT 0
)