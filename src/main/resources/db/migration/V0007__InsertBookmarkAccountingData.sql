UPDATE bookmarks
SET created_on = '2018-01-01', created_by = 'John Smith', last_updated = '2018-02-02'
WHERE bookmark_id = 1;

UPDATE bookmarks
SET created_on = '2018-01-02'
WHERE bookmark_id = 2;

UPDATE bookmarks
SET created_on = '2018-01-03', created_by = 'John Smith'
WHERE bookmark_id = 3;

UPDATE bookmarks
SET created_on = '2018-01-04', created_by = 'Jane Doe', last_updated = '2018-02-02'
WHERE bookmark_id = 4;

UPDATE bookmarks
SET created_on = '2018-01-05', last_updated = '2018-02-02'
WHERE bookmark_id = 5;

UPDATE bookmarks
SET created_on = '2018-01-06', created_by = 'Gary Johnson', last_updated = '2018-02-01'
WHERE bookmark_id = 6;

UPDATE bookmarks
SET created_on = '2018-01-07'
WHERE bookmark_id = 7;

UPDATE bookmarks
SET created_on = '2018-01-10'
WHERE bookmark_id = 10;