package com.dph.bloggit.accessors;

import com.dph.bloggit.domains.DomainBookmark;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IBookmarkAccessor extends JpaRepository<DomainBookmark, Long> {

    List<DomainBookmark> findAllByIsVisible(boolean isVisible);

    List<DomainBookmark> findAllByOrderByPriorityDesc();
}
