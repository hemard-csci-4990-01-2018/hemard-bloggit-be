package com.dph.bloggit.converters;

import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.views.ViewBookmark;

public interface IBookmarkConverter {

    DomainBookmark viewToDomain(ViewBookmark viewBookmark);

    ViewBookmark domainToView(DomainBookmark domainBookmark);
}
