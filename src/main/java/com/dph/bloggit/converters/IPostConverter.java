package com.dph.bloggit.converters;

import com.dph.bloggit.domains.DomainPost;
import com.dph.bloggit.views.ViewPost;

public interface IPostConverter {

    DomainPost viewToDomain(ViewPost viewPost);

    ViewPost domainToView(DomainPost domainPost);
}
