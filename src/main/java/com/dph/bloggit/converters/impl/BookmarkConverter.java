package com.dph.bloggit.converters.impl;

import com.dph.bloggit.accessors.IBookmarkAccessor;
import com.dph.bloggit.converters.IBookmarkConverter;
import com.dph.bloggit.converters.ILocalDateTimeConverter;
import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.engines.IBookmarkEngine;
import com.dph.bloggit.views.ViewBookmark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class BookmarkConverter implements IBookmarkConverter {

    @Autowired
    private ILocalDateTimeConverter localDateTimeConverter;

    @Autowired
    private IBookmarkAccessor bookmarkAccessor;

    @Autowired
    private IBookmarkEngine bookmarkEngine;

    @Override
    public DomainBookmark viewToDomain(ViewBookmark viewBookmark) {
        DomainBookmark domainBookmark = new DomainBookmark();
        domainBookmark.setBookmarkID(viewBookmark.getBookmarkID());
        domainBookmark.setTitle(viewBookmark.getTitle());
        domainBookmark.setUrl(viewBookmark.getUrl());
        domainBookmark.setVisible(viewBookmark.isVisible());
        domainBookmark.setPriority(viewBookmark.getPriority());
        domainBookmark.setCreatedBy(viewBookmark.getCreatedBy());

        LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
        domainBookmark.setLastUpdated(now);

        if (viewBookmark.getBookmarkID() == null) {
            domainBookmark.setCreatedOn(now);
        } else {
            domainBookmark.setCreatedOn(bookmarkAccessor
                    .findOne(viewBookmark.getBookmarkID()).getCreatedOn());
        }
        return domainBookmark;
    }

    @Override
    public ViewBookmark domainToView(DomainBookmark domainBookmark) {
        ViewBookmark viewBookmark = new ViewBookmark();
        viewBookmark.setBookmarkID(domainBookmark.getBookmarkID());
        viewBookmark.setTitle(domainBookmark.getTitle());
        viewBookmark.setUrl(domainBookmark.getUrl());
        viewBookmark.setVisible(domainBookmark.isVisible());
        viewBookmark.setPriority(domainBookmark.getPriority());
        viewBookmark.setCreatedBy(domainBookmark.getCreatedBy());

        viewBookmark.setLastUpdated(localDateTimeConverter
                .convertLocalDateTimeToLong(domainBookmark.getLastUpdated()));

        viewBookmark.setFullUrl(bookmarkEngine.getFullUrl(domainBookmark.getUrl()));
        return viewBookmark;
    }
}
