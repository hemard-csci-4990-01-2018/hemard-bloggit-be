package com.dph.bloggit.domains;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "bookmarks")
public class DomainBookmark {

    @Id
    @Column(name = "bookmark_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookmarkID;

    @Column
    private String title;

    @Column
    private String url;

    @Column(name = "is_visible")
    private boolean isVisible;

    @Column
    private int priority;

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "last_updated")
    private LocalDateTime lastUpdated;

    public Long getBookmarkID() {
        return bookmarkID;
    }

    public void setBookmarkID(Long bookmarkID) {
        this.bookmarkID = bookmarkID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainBookmark that = (DomainBookmark) o;
        return isVisible == that.isVisible &&
                priority == that.priority &&
                Objects.equals(bookmarkID, that.bookmarkID) &&
                Objects.equals(title, that.title) &&
                Objects.equals(url, that.url) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(lastUpdated, that.lastUpdated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bookmarkID, title, url, isVisible, priority, createdOn, createdBy, lastUpdated);
    }

    @Override
    public String toString() {
        return "DomainBookmark{" +
                "bookmarkID=" + bookmarkID +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", isVisible=" + isVisible +
                ", priority=" + priority +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
