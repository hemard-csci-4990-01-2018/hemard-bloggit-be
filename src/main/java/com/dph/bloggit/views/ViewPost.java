package com.dph.bloggit.views;

import java.util.Objects;

public class ViewPost {

    private Long postId;

    private String title;

    private String body;

    private String author;

    private Long createdOn;

    private Long lastUpdated;

    private String summary;

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewPost viewPost = (ViewPost) o;
        return Objects.equals(postId, viewPost.postId) &&
                Objects.equals(title, viewPost.title) &&
                Objects.equals(body, viewPost.body) &&
                Objects.equals(author, viewPost.author) &&
                Objects.equals(createdOn, viewPost.createdOn) &&
                Objects.equals(lastUpdated, viewPost.lastUpdated) &&
                Objects.equals(summary, viewPost.summary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, title, body, author, createdOn, lastUpdated, summary);
    }

    @Override
    public String toString() {
        return "ViewPost{" +
                "postId=" + postId +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", author='" + author + '\'' +
                ", createdOn=" + createdOn +
                ", lastUpdated=" + lastUpdated +
                ", summary='" + summary + '\'' +
                '}';
    }
}
