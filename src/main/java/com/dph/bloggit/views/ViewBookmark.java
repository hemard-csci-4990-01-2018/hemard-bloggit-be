package com.dph.bloggit.views;

import java.util.Objects;

public class ViewBookmark {

    private Long bookmarkID;

    private String title;

    private String url;

    private String fullUrl;

    private boolean isVisible;

    private int priority;

    private String createdBy;

    private Long lastUpdated;

    public Long getBookmarkID() {
        return bookmarkID;
    }

    public void setBookmarkID(Long bookmarkID) {
        this.bookmarkID = bookmarkID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ViewBookmark that = (ViewBookmark) o;
        return isVisible == that.isVisible &&
                priority == that.priority &&
                Objects.equals(bookmarkID, that.bookmarkID) &&
                Objects.equals(title, that.title) &&
                Objects.equals(url, that.url) &&
                Objects.equals(fullUrl, that.fullUrl) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(lastUpdated, that.lastUpdated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bookmarkID, title, url, fullUrl, isVisible, priority, createdBy, lastUpdated);
    }

    @Override
    public String toString() {
        return "ViewBookmark{" +
                "bookmarkID=" + bookmarkID +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", fullUrl='" + fullUrl + '\'' +
                ", isVisible=" + isVisible +
                ", priority=" + priority +
                ", createdBy='" + createdBy + '\'' +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
