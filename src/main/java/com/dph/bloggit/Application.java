package com.dph.bloggit;

import com.dph.bloggit.accessors.IBookmarkAccessor;
import com.dph.bloggit.accessors.IPostAccessor;
import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.domains.DomainPost;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;


@SpringBootApplication
public class Application {

    private static final org.slf4j.Logger LOGGER =
            LoggerFactory.getLogger(Application.class);

    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }
    
}
