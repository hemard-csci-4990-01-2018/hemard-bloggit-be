package com.dph.bloggit.controllers;

import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.managers.IBookmarkManager;
import com.dph.bloggit.views.ViewBookmark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/bookmark")
public class BookmarkController {

    @Autowired
    private IBookmarkManager bookmarkManager;

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<List<ViewBookmark>> getAllBookmarks() {
        return new ResponseEntity<>(bookmarkManager.getAllBookmarks(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{bookmarkId}", method = RequestMethod.GET)
    ResponseEntity<ViewBookmark> getBookmark(@PathVariable Long bookmarkId) {
        return new ResponseEntity<>(bookmarkManager.getBookmarkById(bookmarkId), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    ResponseEntity<List<ViewBookmark>> getVisibleBookmarks(
            @RequestParam("isVisible") Boolean isVisible) {
        return new ResponseEntity<>(bookmarkManager.getVisibleBookmarks(isVisible), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<ViewBookmark> createBookmark(@RequestBody ViewBookmark viewBookmark) {
        return new ResponseEntity<>(bookmarkManager.createBookmark(viewBookmark), HttpStatus.OK);
    }

    @RequestMapping(value = "/{bookmarkId}", method = RequestMethod.PUT)
    ResponseEntity<ViewBookmark> updateBookmark(
            @PathVariable Long bookmarkId, @RequestBody ViewBookmark viewBookmark) {
        return new ResponseEntity<>(bookmarkManager.updateBookmark(bookmarkId, viewBookmark), HttpStatus.OK);
    }

    @RequestMapping(value = "/{bookmarkId}", method = RequestMethod.DELETE)
    ResponseEntity<ViewBookmark> deleteBookmark(@PathVariable Long bookmarkId) {
        return new ResponseEntity<>(bookmarkManager.deleteBookmark(bookmarkId), HttpStatus.OK);
    }
}
