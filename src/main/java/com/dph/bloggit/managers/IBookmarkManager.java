package com.dph.bloggit.managers;

import com.dph.bloggit.views.ViewBookmark;

import java.util.List;

public interface IBookmarkManager {

    List<ViewBookmark> getAllBookmarks();

    ViewBookmark getBookmarkById(Long bookmarkId);

    List<ViewBookmark> getVisibleBookmarks(Boolean isVisible);

    ViewBookmark createBookmark(ViewBookmark viewBookmark);

    ViewBookmark updateBookmark(Long bookmarkId, ViewBookmark viewBookmark);

    ViewBookmark deleteBookmark(Long bookmarkId);
}
