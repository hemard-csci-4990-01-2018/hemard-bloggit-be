package com.dph.bloggit.managers.impl;

import com.dph.bloggit.accessors.IBookmarkAccessor;
import com.dph.bloggit.converters.IBookmarkConverter;
import com.dph.bloggit.domains.DomainBookmark;
import com.dph.bloggit.managers.IBookmarkManager;
import com.dph.bloggit.views.ViewBookmark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookmarkManager implements IBookmarkManager {

    @Autowired
    private IBookmarkAccessor bookmarkAccessor;

    @Autowired
    private IBookmarkConverter bookmarkConverter;

    @Override
    public List<ViewBookmark> getAllBookmarks() {
        return bookmarkAccessor.findAll().stream()
                .map(bookmarkConverter::domainToView)
                .collect(Collectors.toList());
    }

    @Override
    public ViewBookmark getBookmarkById(Long bookmarkId) {
        DomainBookmark domainBookmark = bookmarkAccessor.findOne(bookmarkId);
        if (domainBookmark == null) {
            throw new EntityNotFoundException("Unable to retrieve bookmark: " +
                    bookmarkId.toString());
        }
        return bookmarkConverter.domainToView(domainBookmark);
    }

    @Override
    public List<ViewBookmark> getVisibleBookmarks(Boolean isVisible) {
        return bookmarkAccessor.findAllByIsVisible(isVisible).stream()
                .map(bookmarkConverter::domainToView)
                .collect(Collectors.toList());
    }

    @Override
    public ViewBookmark createBookmark(ViewBookmark viewBookmark) {
        return bookmarkConverter.domainToView(
                bookmarkAccessor.save(
                        bookmarkConverter.viewToDomain(viewBookmark)));
    }

    @Override
    public ViewBookmark updateBookmark(Long bookmarkId, ViewBookmark viewBookmark) {
        DomainBookmark currentBookmark = bookmarkAccessor.findOne(bookmarkId);
        if (currentBookmark == null) {
            throw new EntityNotFoundException("Unable to retrieve bookmark : " +
                    bookmarkId.toString());
        } else if (!viewBookmark.getBookmarkID().equals(bookmarkId)) {
            throw new InvalidParameterException("Provided bookmark id: " +
                    bookmarkId.toString() + " does not match provided bookmark: " +
                    viewBookmark);
        }
        return bookmarkConverter.domainToView(
                bookmarkAccessor.save(
                        bookmarkConverter.viewToDomain(viewBookmark)));
    }

    @Override
    public ViewBookmark deleteBookmark(Long bookmarkId) {
        DomainBookmark domainBookmark = bookmarkAccessor.findOne(bookmarkId);
        if (domainBookmark == null) {
            throw new EntityNotFoundException("Unable to retrieve bookmark: " +
                    bookmarkId.toString());
        }
        bookmarkAccessor.delete(bookmarkId);
        return bookmarkConverter.domainToView(domainBookmark);
    }
}
