package com.dph.bloggit.engines;

public interface IPostEngine {

    String getSummaryText(String text);
}
