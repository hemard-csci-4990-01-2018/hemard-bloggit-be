package com.dph.bloggit.engines.impl;

import com.dph.bloggit.engines.IBookmarkEngine;
import org.springframework.stereotype.Service;

@Service
public class BookmarkEngine implements IBookmarkEngine {
    final String PREFIX = "http://";

    @Override
    public String getFullUrl(String providedUrl) {
        String lowerCaseProvidedUrl = providedUrl.toLowerCase();
        if (lowerCaseProvidedUrl.startsWith(PREFIX)) {
            return lowerCaseProvidedUrl;
        }
        return "" + PREFIX + lowerCaseProvidedUrl;
    }
}
