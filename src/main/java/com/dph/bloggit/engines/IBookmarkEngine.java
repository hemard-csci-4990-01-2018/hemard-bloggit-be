package com.dph.bloggit.engines;

public interface IBookmarkEngine {

    String getFullUrl (String partialUrl);
}
